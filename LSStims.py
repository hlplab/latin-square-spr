#!/usr/bin/env python

#
# Author: Andrew Watts <awatts@bcs.rochester.edu>
#
# Copyright (c) 2014, Andrew Watts and
#        the University of Rochester BCS Department
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

from __future__ import print_function

import logging
from os import getcwd
from random import randint
from random import shuffle
from itertools import chain
from copy import deepcopy
from operator import itemgetter
from excelspr import read_xlsx, read_excel
from excelspr import write_spr
from tagged_spr_parser import parse_tagged_sentence


def wrap_inc(num, max=4):
    """Returns next number in sequence [1..max], looping at max."""
    if num == max:
        return 1
    else:
        if num > max:
            return ((num % max) + 1)
        else:
            return num + 1


def format_region(region):
    """Take a Region namedtuple and return a 'region:start,stop' string."""
    if isinstance(region, list):
        return ''
    else:
        return "{}:{},{}".format(region.tag, region.start, region.stop)


def create_lists(experiment, excel_file):
    """
    """
    # Read in stimulus list (Excel file) as a list of dictionaries
    xls_stims = read_excel(excel_file)
    if len(xls_stims) != 1:
        logging.error('There should only be one sheet!')

    # Get just the contents of the sheet
    stims = xls_stims[0]['contents']

    # 'ItemName' should be an integer (at least for BCS152 purposes), so convert
    # it to an int for correct sorting later
    for item in stims:
        item['ItemName'] = int(item['ItemName'])

    # Build a set of conditions, then make a dict, assigning a number to each
    condset = set([x['Condition'] for x in stims if x['Condition'] != '-'])
    conds = dict(zip(range(1, len(condset) + 1), condset))
    revconds = {v: k for k, v in conds.items()}

    # pick out the fillers and get their item numbers
    fillers = [x for x in stims if x['Experiment'] == 'filler']
    # fill_nums = list(set([x['ItemName'] for x in fillers]))

    # pick out the experimental items and get their item numbers
    expt_items = [x for x in stims if x['Experiment'] != 'filler']
    expt_nums = list(set([x['ItemName'] for x in expt_items]))

    # Replace tagged experiment sentences with untagged
    # and create dict for condtion file
    regions = {}
    for item in expt_items:
        original, sentence, wordlist, tags = parse_tagged_sentence(item['Sentence'])
        regions[item['ItemName']] = tags
        item['Sentence'] = sentence

    for item in fillers:
        original, sentence, wordlist, tags = parse_tagged_sentence(item['Sentence'])
        regions[item['ItemName']] = tags
        item['Sentence'] = sentence

    # Check that there are at least 2x fillers than experimental items
    fill_len = len(fillers)
    expt_len = int(len(expt_items) / len(condset))
    if fill_len < expt_len * 2:
        logging.warn('There are only {} fillers; you should have at least {}'.format(fill_len, expt_len * 2))
    if fill_len <= expt_len:
        logging.error('There are only {} fillers; you MUST have at least {}'.format(fill_len, expt_len + 1))

    # check that for each item number there is an item for each condition
    # for each item number, do a list comprehension of cond names, then use
    # set difference to confirm they match the known conds
    for x in expt_nums:
        xconds = set([z['Condition'] for z in [y for y in expt_items if y['ItemName'] == x]])
        if xconds ^ condset:
            logging.error('Item {} has conditions: {}, should have {}'.format(x, xconds, condset))

    # Make a dictionary keyed on condition with item lists
    cond_lists = {c: [x for x in expt_items if x['Condition'] == c] for c in condset}

    # Get the indices to sample from each list for list 1
    square_indices = {conds[i + 1]: range(1, expt_len + 1)[i::len(condset)] for i in range(len(condset))}

    # Generate one list of experimental items
    list_square = []
    for c in condset:
        list_square.extend([x for x in cond_lists[c] if int(x['ItemName']) in square_indices[c]])

    # Shuffle the items before we generate the final list
    shuffle(list_square)
    shuffle(fillers)

    # FIXME: now handles no fillers, filler count >= experimental item count
    # but not filler < experimental item count
    if fill_len > 0:
        # Start filler insertion by putting them every other item
        list_square = list(chain.from_iterable(zip(list_square, fillers[:expt_len])))

        if fill_len > expt_len:
            # Insert a filler at the beginning and end of the list
            list_square.insert(0, fillers[expt_len])

            # Then randomly insert fillers until we're out
            for f in fillers[expt_len + 1:]:
                while 1:
                    insert_idx = randint(0, len(list_square) - 1)
                    # take a look at a slice 2 back and forward and make sure
                    # there aren't 3 or more fillers
                    if [x['Experiment'] for x in list_square[insert_idx - 1:insert_idx + 3]].count('filler') < 3:
                        list_square.insert(insert_idx, f)
                        break

    # Find the indices of each of the experimental items for each condition
    item_conds = [(x['ItemName'], x['Condition']) for x in list_square]
    cond_indices = {c: [dict(zip(('Index', 'ItemName'), (i, x[0]))) for i, x in enumerate(item_conds) if x[1] == c] for c in condset}

    # Start off our final lists by making the first one a copy of the base list
    final_lists = [deepcopy(list_square)]

    oldlist = list_square
    for j in range(len(conds) - 1):
        newlist = deepcopy(oldlist)
        for k, v in cond_indices.items():
            new_cond = conds[wrap_inc(revconds[k] + j, len(condset))]
            items = sorted(v, key=itemgetter('Index'))

            replacements = [x for x in cond_lists[new_cond] if x['ItemName'] in [y['ItemName'] for y in items]]
            for i in items:
                idx, itm_name = i['Index'], i['ItemName']
                # Gets the matching item or None (should always match)
                # XXX: this is really inefficient; searches whole list every time
                newlist[idx] = next((x for x in replacements if x['ItemName'] == itm_name), None)
        final_lists.append(newlist)
        oldlist = newlist

    # XXX: temp code to check lists were updated correctly
    # zip(*[[y['Condition'] for y in x] for x in final_lists])

    # create reversed lists by mapping reverse over existing and then appending result?
    rev_lists = [list(x) for x in map(reversed, final_lists)]

    # And then add the reversed lists to the final lists
    final_lists.extend(rev_lists)

    # Make a list of dicts with listname and contents
    outdata = []
    for i, stimlist in enumerate(final_lists, 1):
        outdata.append({'sheet': 'list{}'.format(i), 'contents': stimlist})

    # Finally, write them out to the stim files
    # FIXME: starting point should be configurable, not just getcwd()
    write_spr(outdata, experiment, getcwd())

    with open('{}.regions.txt'.format(experiment), 'w') as regionfile:
        for k, v in sorted(regions.items()):
            print('{}|{}'.format(k, ';'.join([format_region(x) for x in v])), file=regionfile)

if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description='Read in an Excel file with SPR\
    stimuli and output latin square lists')
    parser.add_argument('-e', '--experiment', help='The name of the experiment\
    which will be used to name the directory where the stimuli and region mapping\
    files will be put')
    parser.add_argument('-x', '--excel', help='Filename of the input Excel file')
    args = parser.parse_args()

    create_lists(args.experiment, args.excel)
