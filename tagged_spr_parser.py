#!/usr/bin/env python

#
# Author: Andrew Watts <awatts@bcs.rochester.edu>
#
# Copyright (c) 2014, Andrew Watts and
#        the University of Rochester BCS Department
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

from collections import namedtuple
from pyparsing import Literal, Word, OneOrMore, Group, ParseException

ParsedSentence = namedtuple('ParsedSentence', ['original', 'parsed', 'wordlist', 'tags'])
Region = namedtuple('Region', ['tag', 'start', 'stop'])

# pyparsing.printables contains @, {, and }
restricted_printables = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!"#$%&\'()*+,-./:;<=>?[\\]^_`|~'

openblock = Literal('{').suppress()
closeblock = Literal('}').suppress()
tagmark = Literal('@')
# word = Word(alphanums + "'" + "." + "," + ":")
word = Word(restricted_printables)
taggedblock = openblock + OneOrMore(word) + closeblock + tagmark + word
taggedword = word + tagmark + word
wordtoken = taggedblock | taggedword | word
sentence = OneOrMore(Group(wordtoken))

test_sentence = "{This sentence}@subject has@verb plain words and {multiple blocks}@object"


def test_tagged_sentence(insent):
    """Try to parse a region tagged sentence returning true on success, false on exception."""
    # Replace smart quote single quotes with a proper apostrophe
    insent = insent.replace(u'\u2018', "'").replace(u'\u2019', "'")
    try:
        parse_list = sentence.parseString(insent).asList()
        return True
    except ParseException as e:
        return False


def parse_tagged_sentence(insent):
    """
    Parse a potentially region tagged sentence and return the untagged sentence
    and the zero-indexed region positions
    """
    words = []
    tags = []
    pos = 0
    # Replace smart quote single quotes with a proper apostrophe
    insent = insent.replace(u'\u2018', "'").replace(u'\u2019', "'")
    try:
        parse_list = sentence.parseString(insent).asList()
    # print(insent)
    # print(parse_list)
    except ParseException as e:
        # print("Problem with: {}".format(insent))
        # print("ParseException({0}): {1}".format(e.lineno, e.msg))
        return ParsedSentence(insent, "Problem with: {}; {}".format(insent, e.msg), ['', ], [])

    for s in parse_list:
        if len(s) == 1:  # plain word
            words.append(s[0])
            pos += 1
        else:
            tagged_words = s[:s.index('@')]
            lenwords = len(tagged_words)
            # tags.append('{}:{},{}'.format(s[-1], pos, pos + lenwords))
            tags.append(Region(s[-1], pos, pos + lenwords))
            pos += lenwords
            words.extend(tagged_words)
    return ParsedSentence(insent, ' '.join(words), words, tags)

# for y in ["<td>{}</td>".format(x) for x in s0.sentence.split()]:
#     print y

# from operator import attrgetter
# for t in sorted(s0.tags, key=attrgetter('start')):
#     print("<td colspan=\"{}\">{}</td>".format(t.stop-t.start, t.tag))
