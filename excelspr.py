#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
from os import makedirs, mkdir
import os.path
from operator import itemgetter
import xlrd
import xlwt
import openpyxl
from openpyxl import load_workbook
from openpyxl.cell import get_column_letter
from openpyxl.styles import Font, Style
import pandas as pd

config_template = """set	randomize	false
set	insertBreaks	true
instruct	Thank you for taking part in our study!
instruct	In this task you'll read simple sentences and answer yes/no questions about them.<br/>Instead of seeing a whole sentence straight away, you will just see a word at a time. You can read the sentence at your own pace by pressing the space bar when you are ready to see the next word.
instruct	To start with, all you will see is a horizontal line. This line has a sentence "hidden" behind it.<br/><br/>To read the sentence, you will have to press the space bar. This will make the line disappear piece by piece, so that you can see the hidden words.<br/><br/>Every time a new word is revealed, the previous word will be hidden again. There is no way to return to words you have already read once they are hidden, so please read carefully.
instruct	Please read silently, not aloud. Once you have read each sentence, we will ask you question about it.<br/><br/>When you're ready to try some practice sentences, click "OK".	So far so good? Y
practice	This sentence is a practice sentence to help you get used to a moving window display.	So far so good?	Y
practice	This sentence is also for practice, and is followed by a question.	Are you following this so far?	Y
practice	Now you will get to see what happens if you answer a question incorrectly.	Press the "yes" button now, ok?	N
instruct	OK, that's the end of the instructions! You'll get used to it quickly.<br/><br/>When you click OK, you can start reading the first "real" sentence. Please read carefully, and do your best to answer the questions correctly!<br/><br/>We will not be able to pay for HITs that are submitted incomplete or with more than a quarter of questions answered incorrectly.<br/><br/>Good luck!
items	{}
end	Thanks for participating. Please take a moment to give us feedback on this experiment in the box below.  Then click on the "Submit" button below to complete the HIT and return to Mechanical Turk. Goodbye!"""


# Modified from https://github.com/python-excel/xlwt/blob/master/xlwt/examples/xlwt_easyxf_simple_demo.py
ezxf = xlwt.easyxf
heading_xf = ezxf('font: bold on; align: wrap on, vert centre, horiz center')

# Should cut down some I don't need / figure out formats I might want
kind_to_xf_map = {
    'date': ezxf(num_format_str='yyyy-mm-dd'),
    'int': ezxf(num_format_str='#,##0'),
    'money': ezxf('font: italic on; pattern: pattern solid, fore-colour grey25',
                  num_format_str='$#,##0.00'),
    'price': ezxf(num_format_str='#0.000000'),
    'text': ezxf(),
}

bold_style = Style(font=Font(bold=True))
# End stuff from xlwt example


def write_xls_sheet(book, sheet_name, headings, data, heading_xf, data_xfs):
    """
    Write a Worksheet for an XLS (Office 2003/2004 and earlier) Workbook
    """

    sheet = book.add_sheet(sheet_name)
    rowx = 0
    for colx, value in enumerate(headings):
        sheet.write(rowx, colx, value, heading_xf)
    sheet.set_panes_frozen(True)  # frozen headings instead of split panes
    sheet.set_horz_split_pos(rowx + 1)  # in general, freeze after last heading row
    sheet.set_remove_splits(True)  # if user does unfreeze, don't leave a split there
    for row in data:
        rowx += 1
        for colx, value in enumerate(row):
            sheet.write(rowx, colx, value, data_xfs[colx])


def write_xlsx_sheet(book, sheet_name, headings, data):
    """
    Write a Worksheet for an XLSX (Office 2007/2008 and later) Workbook
    """
    if len(book.get_sheet_names()) == 1 and book.active.title == 'Sheet':
        sheet = book.active
    else:
        sheet = book.create_sheet()
    rowx = 1
    sheet.title = sheet_name
    for colx, value in enumerate(headings, 1):
        col = get_column_letter(colx)
        sheet['{}1'.format(col)] = value
        sheet['{}1'.format(col)].style = bold_style
    for row in data:
        rowx += 1
        for colx, value in enumerate(row, 1):
            col = get_column_letter(colx)
            sheet['{}{}'.format(col, rowx)] = value
    # Freezing with openpyxl is weird; freezes everything above and to left of
    # specified cell, so to do top row, must say B2
    c = sheet['B2']
    sheet.freeze_panes = c


# XXX: Code for reading Excel and outputing SPR
# XXX: xlrd code
def read_xls(wbname):
    workbook = xlrd.open_workbook('{}.xls'.format(wbname))
    sheets = workbook.sheet_names()
    allsheets = []
    for sheet in sheets:
        sheetrows = []
        currsheet = workbook.sheet_by_name(sheet)
        header_row = 0
        headers = [currsheet.cell_value(header_row, i) for i in xrange(currsheet.ncols)]
        # TODO: at this point the keys in the header should be validated
        # to ensure they are in {'Experiment','ItemName','Condition',
        # 'SentenceN', 'QuestionN', 'AnswerN'}
        for row in xrange(header_row+1, currsheet.nrows):
            sheetrows.append(dict(zip(headers, currsheet.row_values(row))))
        allsheets.append({'sheet': sheet, 'contents': sheetrows})
    return allsheets


# XXX: openpyxl
def read_xlsx(wbname):
    """
    Read in an XLSX (Office 2007/2008 and later) Workbook and return a list
    of dicts with sheetnames as keys, where each dict has as its value a
    list of dicts with values of each row
    """
    workbook = load_workbook('{}.xlsx'.format(wbname))
    sheets = workbook.get_sheet_names()
    allsheets = []
    for sheet in sheets:
        sheetrows = []
        currsheet = workbook.get_sheet_by_name(sheet)
        headers = [c.value for c in currsheet.rows[0]]
        # TODO: at this point the keys in the header should be validated
        # to ensure they are in {'Experiment','ItemName','Condition',
        # 'SentenceN', 'QuestionN', 'AnswerN'}
        for row in currsheet.rows[1:]:
            sheetrows.append(dict(zip(headers, [c.value for c in row])))
        allsheets.append({'sheet': sheet, 'contents': sheetrows})
    return allsheets


def read_excel(wbname):
    """Use pandas to read in an Excel sheet."""
    allsheets = []
    workbook = pd.read_excel(wbname, None)  # None gets all sheets
    for k, v in workbook.items():
        sheet_dict = {}
        sheet_dict['sheet'] = k
        # Start by dropping everything after the 'Answer' column
        # This works for BCS152, but is less flexible for experiments that have
        # multiple Question and Answer columns
        v.drop(v.columns[6:], inplace=True, axis=1)
        sheet_dict['contents'] = [dict(row) for i, row in v.iterrows()]
        allsheets.append(sheet_dict)
    return allsheets


# XXX: Code for reading SPR files and outputing Excel
def read_spr(sprfile):
    """
    Read in a Linger-like SPR stimuli file and turn it into a list of
    dicts of stimuli items and keys for column names
    """
    stims = []
    keys = set()
    with open(sprfile, 'r') as spr:
        currstim = {}
        snum, qnum = 1, 1
        for line in spr:
            if line[0] == '#':
                if currstim != {}:
                    stims.append(currstim)
                currstim = dict(zip(('Experiment', 'ItemName', 'Condition'),
                                    line.strip().split()[1:]))
                snum, qnum = 1, 1
            elif line[0] == '?':
                qstr = 'Question{}'.format(qnum)
                astr = 'Answer{}'.format(qnum)
                currstim[qstr] = line.strip()[1:-1]
                currstim[astr] = line.strip()[-1]
                keys.add(qstr)
                keys.add(astr)
                qnum += 1
            elif line.strip() == '':
                pass
            else:  # should only be sentences
                sstr = 'Sentence{}'.format(snum)
                currstim[sstr] = line.strip()
                keys.add(sstr)
                snum += 1
    return (stims, keys)


def write_spr(sprdata, dirname, basepath):
    """
    Given a list of dicts with sheetnames as keys, where each dict has as its
    value a list of dicts with values of each row, output a directory of
    Linger-style stimuli files, one per dict
    """

    # create directory with dirname
    fullpath = os.path.join(basepath, dirname)
    if not os.path.exists(fullpath):
        makedirs(fullpath)

    for row in sprdata:
        sheet = row['sheet']
        contents = row['contents']

        sprdir = os.path.join(fullpath, dirname + sheet)
        if not os.path.exists(sprdir):
            mkdir(sprdir)

        with open(os.path.join(sprdir, "{}.itm".format(sheet)), 'w') as sprfile:
            for item in contents:
                print("# {} {} {}".format(item['Experiment'],
                                          item['ItemName'],
                                          item['Condition'].lower().replace(' ', '_')), file=sprfile)
                # To get all questions (or answers, etc.). returns list of tuples
                sentences = sorted(
                    [(key, value) for key, value in item.items() if key.startswith("Sentence")],
                    key=itemgetter(0))
                for sent in sentences:
                    print(sent[1], file=sprfile)

                questions = sorted(
                    [(key, value) for key, value in item.items() if key.startswith("Question")],
                    key=itemgetter(0))
                answers = sorted(
                    [(key, value) for key, value in item.items() if key.startswith("Answer")],
                    key=itemgetter(0))
                for quest, ans in zip([q[1] for q in questions], [a[1] for a in answers]):
                    print("? {} {}".format(quest, ans), file=sprfile)

        with open(os.path.join(sprdir, 'config'), 'w') as configfile:
            print(config_template.format("{}.itm".format(sheet)), file=configfile)


def generate_column_names(sprkeys):
    """
    Create appropriately named column names based on data in the SPR input file
    """
    colnames = ['Experiment', 'ItemName', 'Condition']
    i = 1
    while len(sprkeys) > 0:  # dangerous territory, but I think I can trust me
        si = 'Sentence{}'.format(i)
        qi = 'Question{}'.format(i)
        ai = 'Answer{}'.format(i)
        for k in (si, qi, ai):
            if k in sprkeys:
                colnames.append(k)
                sprkeys.remove(k)
            i += 1
    return colnames


def format_data_for_excel(sprstims, colnames):
    """
    Take the rows of SPR stimuli and put them in the order of the column names,
    filling in any empty values with the empty string
    """
    data = []
    for row in sprstims:
        for k in colnames:
            if k not in row:
                row[k] = ''
        data.append([row[x] for x in colnames])
    return data


def xlsFromSPR(experiment, infiles):
    """
    Create an XLS (Office 2003/2004 or older) file with one sheet per
    experiment list.
    'infiles' should be a list of tuples of listname and filename
    """

    outfile_name = "{}.xls".format(experiment)
    book = xlwt.Workbook(encoding='utf-8')

    for sheetname, filename in infiles:
        sprstims, sprkeys = read_spr(filename)
        colnames = generate_column_names(sprkeys)
        key_xfs = [kind_to_xf_map['text'] for k in colnames]
        data = format_data_for_excel(sprstims, colnames)
        write_xls_sheet(book, sheetname, colnames, data, heading_xf, key_xfs)

    book.save(outfile_name)


def xlsxFromSPR(experiment, infiles):
    """
    Create an XLSX (Office 2007/2008 or newer) file with one sheet per
    experiment list.
    'infiles' should be a list of tuples of listname and filename
    """

    outfile_name = "{}.xlsx".format(experiment)

    book = openpyxl.Workbook()

    for sheetname, filename in infiles:
        sprstims, sprkeys = read_spr(filename)
        colnames = generate_column_names(sprkeys)
        data = format_data_for_excel(sprstims, colnames)
        write_xlsx_sheet(book, sheetname, colnames, data)

    book.save(filename=outfile_name)

if __name__ == '__main__':
    infiles = [
        ('ControlList1', '/resources/mturk/mtSPR/flexspr/ControlList1/cl1.itm'),
        ('ControlList2', '/resources/mturk/mtSPR/flexspr/ControlList2/cl2.itm'),
        ('ControlList3', '/resources/mturk/mtSPR/flexspr/ControlList3/cl3.itm'),
        ('ControlList4', '/resources/mturk/mtSPR/flexspr/ControlList4/cl4.itm'),
        # ('ControlList5', '/resources/mturk/mtSPR/flexspr/ControlList5/cl5.itm'),
        # ('ControlList6', '/resources/mturk/mtSPR/flexspr/ControlList6/cl6.itm'),
        # ('ControlList7', '/resources/mturk/mtSPR/flexspr/ControlList7/cl7.itm'),
        # ('ControlList8', '/resources/mturk/mtSPR/flexspr/ControlList8/cl8.itm'),
    ]

    xlsFromSPR('ControlList', infiles)
    xlsxFromSPR('ControlList', infiles)
